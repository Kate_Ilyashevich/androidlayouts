package com.example.kate.androidlayouts;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by kathe on 16.11.2016.
 */

public class SecondActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }
}
