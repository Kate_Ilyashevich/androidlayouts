package com.example.kate.androidlayouts;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public static final String ACTION_SECOND_ACTIVITY = "com.example.kate.androidlayouts.SecondActivity";
    public static final String ACTION_THIRD_ACTIVITY = "com.example.kate.androidlayouts.ThirdActivity";
    public static final String ACTION_FOURTH_ACTIVITY = "com.example.kate.androidlayouts.FourthActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick1(View view) {
        startActivity(new Intent(ACTION_SECOND_ACTIVITY));
    }
    public void onClick2(View view) {
        startActivity(new Intent(ACTION_THIRD_ACTIVITY));
    }
    public void onClick3(View view) {
        startActivity(new Intent(ACTION_FOURTH_ACTIVITY));
    }
    public void onClick4(View view) {
        this.finish();
    }
}
